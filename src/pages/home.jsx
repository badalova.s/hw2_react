import { Component } from "react";
import PropTypes from 'prop-types';
import List from "../components/list"
import Header from "../header";

export default class Home extends Component{

    constructor(props) {
        super(props);
       this.state={
        orders:[],
        item:[],
        favorite:[]
       }
     this.addToorder=this.addToorder.bind(this)
     this.addTofavorite=this.addTofavorite.bind(this)
      }

      
        render() {
  
          return (
            <>
            <Header curent={this.state.orders.length} favorites={this.state.favorite.length} />
             <List item={this.state.item} onAdd={this.addToorder} favAdd={this.addTofavorite} />
             </>
          );
        }
        addToorder(i) {
          this.setState({orders:[...this.state.orders, i]}, ()=>{localStorage.setItem("cards", JSON.stringify(this.state.orders))})
        }
        addTofavorite(e) {
          
          this.setState({favorite:[...this.state.favorite, e]}, ()=>{localStorage.setItem("favorite", JSON.stringify(this.state.favorite))} )
          
        
               }

}
  
      