export const arr =[
    {
        name:"Ноутбук Acer Aspire 3 A315-58-354Q",
    price:"15 499₴",
    image: "/my-app/public/1.jpg",
    color:"Pure Silver",
    article:"fd5643466"},

    {name:"Ноутбук ASUS TUF Gaming F15 FX506HM-HN017 (90NR0753-M01170) ",
    price:"45 999₴",
    image: "/my-app/public/2.jpg",
    color:"Eclipse Gray",
    article:"6fgh464346"},

    {name:"Ноутбук Acer Aspire 5 A515-45-R2ZN (NX.A7ZEU.002)",
    price:"15 499₴",
    image: "/my-app/public/3.jpg",
    color:"Charcoal Black",
    article:"f5fd5643466"},

    {name:"Ноутбук Lenovo IdeaPad 3 15ITL6 (82H800QPRA)",
    price:"19 999₴",
    image: "/my-app/public/4.jpg",
    color:"Arctic Grey",
    article:"fd564346667"},

    {name:"Ноутбук ASUS VivoBook 15 OLED K513EA-L11653 (90NB0SG1-M01D10)",
    price:"18 499₴",
    image: "/my-app/public/5.jpg",
    color:"Pure Silver",
    article:"fd564346656"},
    
    {name:"Ноутбук Acer Aspire 3 A315-24P-R42V (NX.KDEEU.014)",
    price:"35 499₴",
    image: "/my-app/public/6.jpg",
    color:"Red",
    article:"88fd5643466"},

    {name:"Ноутбук ASUS Vivobook Go 15 E510KA-BQ296 (90NB0UJ5-M00BM0)",
    price:"15 999₴",
    image: "/my-app/public/7.jpg",
    color:"Black",
    article:"fd564344466"},

    {name:"Apple MacBook Pro 14 M2 Pro 512GB 2023 (MPHE3UA/A)",
    price:"16 499₴",
    image: "/my-app/public/8.jpg",
    color:"Pure Silver",
    article:"45fd5643466"},

    {name:"ASUS ZenBook 14X OLED UX3404VC-M9026WS (90NB10H1-M00760)",
    price:"19 499₴",
    image: "/my-app/public/9.jpg",
    color:"Pure Blue",
    article:"fd56435466"},

    {name:"Ноутбук Asus ROG Zephyrus G16 GU603ZV-N4010 (90NR0H23-M002E0)",
    price:"18 499₴",
    image: "/my-app/public/10.jpg",
    color:"Silver",
    article:"55fd5643466"}

]

console.log(JSON.stringify(arr))