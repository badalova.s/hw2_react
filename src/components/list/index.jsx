import { Component } from "react";
import PropTypes from 'prop-types';
import Card from "../card"
import Modal from "../modal"
import "./list.scss"
export default class list extends Component {
    constructor(props) {
      super(props);
      this.state = { isLoading: false, item:[],modalToShow:"", it:"" };
      

  }

  openModal=(active)=>{
    this.setState({modalToShow:active,
      })
  }
componentDidMount() {
    this.setState({ isLoading: true });
    fetch("./list.json")
      .then((res) => res.json())
      .then((data) => {
        setTimeout(() => {
          this.setState({ item:data});
          this.setState({ isLoading: false });
        }, 1500);
      })
 
    }
    
    render(){
     
      const {isLoading, item}=this.state
        
           
              if (isLoading) {
                return(

                
                <div>loading...</div>)
     } else {
      return (
        <>
        
                <div className="list">
                    { item.map((el)=> (
          <Card key={el.id} name={el.name} image={el.image} 
        price={el.price}
        color={el.color}
        article={el.article}
        addDelete={(e)=>{this.props.favAdd(el)
          e.target.src='./img.png'
          }}
        Click={()=>{this.openModal(true) 

        this.setState({it:el}
          )
        
        }
        
      }

        onAdd={this.props.onAdd}
        favAdd={this.props.favAdd}

/>
     
          ))}
         
         </div>
        <Modal 
        // i={this.state.it}

        active={this.state.modalToShow} 
        Click={(e)=> this.openModal(false)}
        add={()=>{this.props.onAdd(this.state.it)
          this.openModal(false)}}
         />

      </>
         )
              }
             

}
}