import { Component } from "react";
import "./modal.scss";

export default class Modal extends Component {
    constructor(props) {
      super(props);
    }
 
    

    render() {
        
       return (
        
        <div className={this.props.active ? "modal active" : "modal"} onClick={this.props.Click}>
        
        <div className={this.props.active ? "modal_content active" : "modal_content"} onClick={e=>e.stopPropagation()}>
        <div className="modal__exit" onClick={this.props.Click}>X</div>
        <div>Do you add to cart this Product?</div>
          
        
        <div className="modal_btn_conteiner">
        <button onClick={this.props.add} className="modal_btn" >OK</button>
        </div>
        </div>
        </div>
      );
    }
  }
  