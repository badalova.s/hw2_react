import { Component } from "react";
import PropTypes from 'prop-types';
import "./card.scss"


export default class Card extends Component {
    constructor(props) {
      super(props);
      
    }
    
  
 
      render() {

        return (
            <div className="card">
            <div className="card__img"><img style={{
            width: "200px",
            height: "130px"
            }} src={this.props.image}></img></div>
            <p className="card__name">{this.props.name}</p>
            <p className="card__price">Ціна: {this.props.price}</p>
            <p className="card__color">Колір: {this.props.color}</p>
            <p className="card__aticle">Артикуль: {this.props.article}</p>
            <div className="card__button">
            <button onClick={this.props.Click}>ADD TO CART</button>
            <img style={{ 
             
            width: "20px",
            height: "20px"
            }}  src="./images.png" onClick={this.props.addDelete}></img>
            </div>
            </div>
            
        )
      }
      
      
    }

    Card.propTypes = {
      name: PropTypes.string,
      color: PropTypes.string,
      price:PropTypes.string,
      article:PropTypes.string,
      Click:PropTypes.func
    }
